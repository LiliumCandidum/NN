import java.io.ByteArrayOutputStream;
import java.io.RandomAccessFile;
import static java.lang.String.format;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * From github
 * https://github.com/jeffgriffith/mnist-reader/blob/master/src/main/java/mnist/MnistReader.java
 */
public class MnistReader 
{
  private static final int IMAGE_FILE_MAGIC_NUMBER = 2051;
  private static final int LABEL_FILE_MAGIC_NUMBER = 2049;
  int[] labels;
  List<int[][]> images;

  public MnistReader(String type)
    {
    if(type.equals("train"))
      {
      labels = MnistReader.getLabels("train-labels.idx1-ubyte");
      images = MnistReader.getImages("train-images.idx3-ubyte");
      }
     else
      {
      labels = MnistReader.getLabels("t10k-labels.idx1-ubyte");
      images = MnistReader.getImages("t10k-images.idx3-ubyte");
      }
    }

  public static int[] getLabels(String infile) 
    {
    ByteBuffer bb = loadFileToByteBuffer(infile);

    assertMagicNumber(LABEL_FILE_MAGIC_NUMBER, bb.getInt());

    int numLabels = bb.getInt();
    int[] labels = new int[numLabels];

    for (int i = 0; i < numLabels; ++i)
      labels[i] = bb.get() & 0xFF; // To unsigned

    return labels;
    }

  public static List<int[][]> getImages(String infile) 
    {
    ByteBuffer bb = loadFileToByteBuffer(infile);

    assertMagicNumber(IMAGE_FILE_MAGIC_NUMBER, bb.getInt());

    int numImages = bb.getInt();
    int numRows = bb.getInt();
    int numColumns = bb.getInt();
    List<int[][]> images = new ArrayList<>();

    for (int i = 0; i < numImages; i++)
      images.add(readImage(numRows, numColumns, bb));

    return images;
    }

  private static int[][] readImage(int numRows, int numCols, ByteBuffer bb) 
    {
    int[][] image = new int[numRows][];
    for (int row = 0; row < numRows; row++)
      image[row] = readRow(numCols, bb);
    return image;
    }

  private static int[] readRow(int numCols, ByteBuffer bb) 
    {
    int[] row = new int[numCols];
    for (int col = 0; col < numCols; ++col)
      row[col] = bb.get() & 0xFF; // To unsigned
    return row;
    }

  public static void assertMagicNumber(int expectedMagicNumber, int magicNumber) 
    {
    if (expectedMagicNumber != magicNumber) 
      {
      switch (expectedMagicNumber) 
        {
        case LABEL_FILE_MAGIC_NUMBER:
	  throw new RuntimeException("This is not a label file.");
        case IMAGE_FILE_MAGIC_NUMBER:
          throw new RuntimeException("This is not an image file.");
        default:
          throw new RuntimeException(
	    format("Expected magic number %d, found %d", expectedMagicNumber, magicNumber));
        }
      }
    }

  public static ByteBuffer loadFileToByteBuffer(String infile) 
  {return ByteBuffer.wrap(loadFile(infile));}

  public static byte[] loadFile(String infile) 
    {
    try 
      {
      RandomAccessFile f = new RandomAccessFile(infile, "r");
      FileChannel chan = f.getChannel();
      long fileSize = chan.size();
      ByteBuffer bb = ByteBuffer.allocate((int) fileSize);
      chan.read(bb);
      bb.flip();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      for (int i = 0; i < fileSize; i++)
        baos.write(bb.get());
      chan.close();
      f.close();
      return baos.toByteArray();
      } catch (Exception e) {
	throw new RuntimeException(e);}
    }

  public double[] getImagePixels(int index)
    {
    int image[][]=images.get(index);
    double imagePixel[]= new double[image.length*image[0].length];
    int count=0;
    for (int row = 0; row < image.length; row++)
      for (int col = 0; col < image[row].length; col++) 
        {
        int pixelVal = image[row][col];
        if (pixelVal == 0)//bianco
          imagePixel[count]=0;
        else if (pixelVal < 256 / 3)//grigio 1
          imagePixel[count]=0.3;
        else if (pixelVal < 2 * (256 / 3))//grigio 2
          imagePixel[count]=0.6;
        else//nero
          imagePixel[count]=1;
        count++;
        }
    return imagePixel;
    }

  public double[] getOutputNumber(int index)
    {
    double output[]=new double[10];
    output[labels[index]]=1;
    return output;
    }    
}