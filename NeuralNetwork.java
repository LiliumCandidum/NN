import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Paola Nicosia
 */
public class NeuralNetwork 
{
   private final int NUM_INPUT=28*28;
   private final int NUM_HIDDEN=20;
   private final int NUM_OUTPUT=10;
   private final double LEARNING_COSTANT=0.1;

   /* weigth connecting the input to the hidden
   i=hidden , j=input */
   private double wIH[][];
   /* weigth connecting the hidden to the output
   i=output , j=hidden */
   private double wHO[][];
   //biases for the output
   private double biasesOut[];
   //every element is the input for the output layer
   private double activationHidden[];
   //every element is the input for the output
   private double activationOutput[];
   //weighted input for the hidden (without sigmoid)
   private double zInHidden[];
   //weighted input for the output (without sigmoid)
   private double zInOutput[];

  public NeuralNetwork()
  {
    wIH=new double[NUM_HIDDEN][NUM_INPUT];
    wHO=new double[NUM_OUTPUT][NUM_HIDDEN];
    biasesOut=new double[NUM_OUTPUT];

    File f=new File("NNFile.txt");
    if(f.exists())
      readFile(f);
    else
    {
      //randomize weight and biases
      for (int i=0; i<wIH.length; i++)
      {
        for(int j=0; j<wIH[i].length; j++)
          wIH[i][j]=(Math.random()*(1/Math.sqrt(NUM_INPUT))+(1/Math.sqrt(NUM_INPUT)))-(1/Math.sqrt(NUM_INPUT));
      }
      for (int i=0; i<wHO.length; i++)
      {
        for(int j=0; j<wHO[i].length; j++)
          wHO[i][j]=(Math.random()*(1/Math.sqrt(NUM_INPUT))+(1/Math.sqrt(NUM_INPUT)))-(1/Math.sqrt(NUM_INPUT));
        biasesOut[i]=(Math.random()*(1/Math.sqrt(NUM_INPUT))+(1/Math.sqrt(NUM_INPUT)))-(1/Math.sqrt(NUM_INPUT));
      }
      train();
      saveWeigths(f);
    }

    //test, poi togli
   /* MnistReader mnistTest=new MnistReader("test");
    for(int i=0; i<10000; i++)
      {
      double[] out=mnistTest.getOutputNumber(i);
      for(int k=0; k<out.length; k++)
        if(out[k]==1)
          System.out.println("Test "+k);
      double[] result = feedforward(mnistTest.getImagePixels(i));
      for(int k=0; k<result.length; k++)
        System.out.println("Num "+k+": "+result[k]);
      }*/
  }

  public double[] feedforward(double[] inputPixel)
  {
    zInHidden=new double[NUM_HIDDEN];
    activationHidden=new double[NUM_HIDDEN];
    for(int hidden=0; hidden<wIH.length; hidden++)
    {
      for(int input=0; input<wIH[hidden].length; input++)
        zInHidden[hidden]+=inputPixel[input]*wIH[hidden][input];
      activationHidden[hidden]=sigmoid(zInHidden[hidden]);
    }

    zInOutput=new double[NUM_OUTPUT];
    activationOutput=new double[NUM_OUTPUT];
    for(int output=0; output<wHO.length; output++)
    {
      for(int hidden=0; hidden<wHO[output].length; hidden++)
        zInOutput[output]+=activationHidden[hidden]*wHO[output][hidden];
      zInOutput[output]+=biasesOut[output];
      activationOutput[output]=sigmoid(zInOutput[output]);
    }
    return activationOutput;//poi fai ritornare solo il numero riconosciuto, -1 se non l'ha riconosciuto
  }

  /**
   * Train the network with online learning
   */
  private void train()
  {
    MnistReader mnistTrain=new MnistReader("train");
    for(int i=0; i<50000; i++)
      backpropagation(mnistTrain.getImagePixels(i), mnistTrain.getOutputNumber(i));
  }

  /**
   * Backpropagation
   * @param x input vector (image)
   * @param y desidered output vector
   */
  private void backpropagation(double[] x, double[] y)
  {
    feedforward(x);

    /***  Backpropagation  ***/
    /*BP1 First equation of backpropagation for the error
     in the  output layer. (y the array of desidered output)*/
    double errorOutput[]=new double[NUM_OUTPUT];
    for(int i=0; i<errorOutput.length; i++)
      errorOutput[i]=(activationOutput[i]-y[i]);//*sigmoid_derivate(zInOutput[i]);

    /*BP2 Second equation, backpropagate the error in
     the hidden layer*/
    double errorHidden[]=new double[NUM_HIDDEN];
    for(int j=0; j<wHO[0].length; j++)//transposed matrix
      for(int i=0; i<wHO.length; i++)
        errorHidden[j]+=wHO[i][j]*errorOutput[i];

    for(int i=0; i<errorHidden.length; i++)
      errorHidden[i]=errorHidden[i]*sigmoid_derivate(zInHidden[i]);

    /*Aggiusta le w e b*/
    for(int hidden=0; hidden<wIH.length; hidden++)
      for(int input=0; input<wIH[hidden].length; input++)
        wIH[hidden][input]-=LEARNING_COSTANT*x[input]*errorHidden[hidden];

    for(int output=0; output<wHO.length; output++)
    {
      for(int hidden=0; hidden<wHO[output].length; hidden++)
        wHO[output][hidden]-=LEARNING_COSTANT*activationHidden[hidden]*errorOutput[output];
      biasesOut[output]-=LEARNING_COSTANT*errorOutput[output];
    }
  }

  private void readFile(File f)
  {
    try
    {
      BufferedReader br=new BufferedReader(new FileReader(f));
      String line="";
      //reading weigths from input to hidden
      for(int i=0; i<wIH.length && line!="\n"; i++)
      {
        line=br.readLine();//blank line
        if(line==null)
        {
          System.out.println("Errore nella lettura del file");
          break;
        }
        String[] currLine=line.split(" ");
        for(int j=0; j<wIH[i].length; j++)
          wIH[i][j]=Double.parseDouble(currLine[j]);
      }
      br.readLine();//blank line
      //reading weigths from hidden to output
      for(int i=0; i<wHO.length && line!="\n"; i++)
      {
        line=br.readLine();
        if(line==null)
        {
          System.out.println("Errore nella lettura del file");
          break;
        }
        String[] currLine=line.split(" ");
        for(int j=0; j<wHO[i].length; j++)
          wHO[i][j]=Double.parseDouble(currLine[j]);
      }
      br.readLine();//blank line
      line=br.readLine();
      if(line==null)
        System.out.println("Errore nella lettura del file");
      else
      {//reading biases
        String[] currLine=line.split(" ");
        for(int i=0; i<biasesOut.length; i++)
          biasesOut[i]=Double.parseDouble(currLine[i]);
      }
      if((line=br.readLine())!=null)
        System.out.println(line);//che si fa??

      br.close();
    }
    catch(IOException ioe)
      {ioe.printStackTrace();}
  }

  /**
   * Save weigths and biases in a file
   */
  private void saveWeigths(File f)
  {
    try
    {
      if(!f.exists())
        f.createNewFile();
      BufferedWriter bw=new BufferedWriter(new FileWriter(f));
      String s="";
      for(int i=0; i<wIH.length; i++)//wIH
      {
        for(int j=0; j<wIH[i].length; j++)
          if(j==wIH[i].length-1)
            s+=wIH[i][j];
           else
            s+=wIH[i][j]+" ";
        bw.write(s);
        bw.newLine();
        s="";
      }
      bw.newLine();
      String b="";
      for(int i=0; i<wHO.length; i++)//wHO e biases
      {
        for(int j=0; j<wHO[i].length; j++)
          if(j==wHO[i].length-1)
            s+=wHO[i][j];
           else
            s+=wHO[i][j]+" ";
        if(i==wHO.length-1)
          b+=biasesOut[i];
         else
          b+=biasesOut[i]+" ";
        bw.write(s);
        bw.newLine();
        s="";
      }
      bw.newLine();
      bw.write(b);

      bw.close();
    }
    catch(IOException ioe)
      {ioe.printStackTrace();}
  }

  private double sigmoid(double z)
  {
    return 1.0/(1.0+Math.exp(-z));
  }
  private double sigmoid_derivate(double z)
  {
    double sigmoidZ=sigmoid(z);
    return sigmoidZ*(1-sigmoidZ);
  }
  
  //Funzione ReLU, anche se c'è scritto sigmoid
 /* private double sigmoid(double z)
  {
    if(z<0)
        return 0;
     return z;
  }
  private double sigmoid_derivate(double z)
  {
    if(z<0)
        return 0;
    return 1;
  }*/
}