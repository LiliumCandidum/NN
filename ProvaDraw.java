import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class ProvaDraw extends JPanel
{
  private int oldx=-1, oldy=-1, x=-1, y=-1;
  private Image img;
  private Graphics2D g2d;

  public ProvaDraw(int width, int height)
  {
    setPreferredSize(new Dimension(width, height));

    addMouseMotionListener(new MouseMotionListener(){
           @Override
           public void mouseMoved(MouseEvent e){}

           @Override
          public void mouseDragged(MouseEvent e)
          {
            x=e.getX();
            y=e.getY();
            g2d.setStroke(new BasicStroke(4));
            g2d.drawLine(oldx,oldy,x, y);
            repaint();
            oldx=x;
            oldy=y;
          }
      });
    addMouseListener(new MouseListener(){
          @Override
          public void mouseReleased(MouseEvent e){}
          @Override
          public void mouseClicked(MouseEvent e) {}
          @Override
          public void mousePressed(MouseEvent e) 
          {
            oldx=e.getX();
            oldy=e.getY();
          }
          @Override
          public void mouseEntered(MouseEvent e) {}
          @Override
          public void mouseExited(MouseEvent e) {}
        });

    }

  @Override
  public void paintComponent(Graphics g) 
  {
    if(img == null)
    {
      img = createImage(getSize().width, getSize().height);
      g2d = (Graphics2D) img.getGraphics();
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      reset();
    }

    g.drawImage(img, 0, 0, null);
  } 

  public void reset()
  {
    g2d.setPaint(Color.white);
    // draw white on entire draw area to clear
    g2d.fillRect(0, 0, getSize().width, getSize().height);
    g2d.setPaint(Color.black);
    repaint();
  }

  public BufferedImage getScaledImg()
  {
    BufferedImage scaledImg = new BufferedImage(28, 28, BufferedImage.TYPE_BYTE_GRAY);
    Graphics2D g = scaledImg.createGraphics();
    g.drawImage(img, 0, 0, 28, 28, null);
    g.dispose();

    return scaledImg;
  }
}