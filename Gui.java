import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class Gui 
{

  public static void main(String[] args) 
  {
    SwingUtilities.invokeLater(new Runnable() {
    public void run() 
    {
      JFrame frame = new JFrame("Prova");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(500, 300);

      JPanel panel=new JPanel();
      panel.setBackground(Color.DARK_GRAY);

      ProvaDraw draw=new ProvaDraw(112, 112);//28*4

      JButton b=new JButton("img");
      b.addActionListener(new ActionListener(){
          @Override
          public void actionPerformed(ActionEvent e)
            {
            BufferedImage b = draw.getScaledImg();
            byte[] grayscale = ((DataBufferByte)b.getRaster().getDataBuffer()).getData();
            double[] input=new double[b.getWidth()*b.getHeight()];
            System.out.println();
            for(int i=0; i<grayscale.length; i++)
            {
              byte sColor = grayscale[i];//signed color
              int unsColor = sColor & 0xFF;//unsigned color
             /* System.out.println(""+unsColor+" i:"+i);
              System.out.println("color? "+sColor+" i: "+i);*/
              if(unsColor == 0)//nero
                input[i] = 1;
              else if(unsColor == 255)//bianco
                input[i] = 0;
              else if(unsColor > 127 && unsColor <= 254)//grigio 1
                input[i] = 0.3;
              else if(unsColor >= 1 && unsColor <= 127)//grigio 2
                input[i] = 0.6;
            }
            NeuralNetwork nn=new NeuralNetwork();
            double[] result = nn.feedforward(input);
            for(int k=0; k<result.length; k++)
              System.out.println("Num "+k+": "+result[k]);

            try 
            {
              ImageIO.write(b, "png", new File("ciao.png"));
            } 
            catch (IOException ex) {}
          }
        });

      JButton c=new JButton("clear");
      c.addActionListener(new ActionListener(){
          @Override
          public void actionPerformed(ActionEvent e)
          {
            draw.reset();
          }
        });
      panel.add(draw);
      panel.add(c);
      panel.add(b);

      frame.add(panel);
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);
    }
  });
  }
}